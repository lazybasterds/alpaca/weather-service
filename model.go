package main

//Weather contains the basic information that is retrieved from http://openweathermap.org
type Weather struct {
	ID         int32         `json:"id"`
	Name       string        `json:"name"`
	Code       int32         `json:"cod"`
	Coord      CoordInfo     `json:"coord"`
	Base       string        `json:"base"`
	Weather    []WeatherInfo `json:"weather"`
	Main       Info          `json:"main"`
	Visibility float64       `json:"visibility"`
	Wind       WindInfo      `json:"wind"`
	Clouds     CloudsInfo    `json:"clouds"`
	DateTime   float64       `json:"dt"`
	System     SystemInfo    `json:"sys"`
}

//CoordInfo contains the coordinates
type CoordInfo struct {
	Latitude  float64 `json:"lat"`
	Longitude float64 `json:"lon"`
}

//WeatherInfo info contains the main weather information in general
type WeatherInfo struct {
	ID          int32  `json:"id"`
	Main        string `json:"main"`
	Description string `json:"description"`
	Icon        string `json:"icon"`
}

//Info contains the information about temperature, pressure, etc.
type Info struct {
	TempInK  float64 `json:"temp"`
	TempInC  float64 `json:"tempinc"`
	TempInF  float64 `json:"tempinf"`
	Pressure float64 `json:"pressure"`
	Humidity float64 `json:"humidity"`
	TempMin  float64 `json:"temp_min"`
	TempMax  float64 `json:"temp_max"`
}

//WindInfo contains the information about wind speed and degree
type WindInfo struct {
	Speed  float64 `json:"speed"`
	Degree float64 `json:"deg"`
}

//CloudsInfo contains the information about the clouds
type CloudsInfo struct {
	All int32 `json:"all"`
}

//SystemInfo contains the information about the system
type SystemInfo struct {
	ID      int32   `json:"id"`
	Type    int32   `json:"type"`
	Message float64 `json:"message"`
	Country string  `json:"country"`
	Sunrise float64 `json:"sunrise"`
	Sunset  float64 `json:"sunset"`
}
